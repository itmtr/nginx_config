cd /home/nginx_config

git fetch --all
git reset --hard origin/master
git pull

checkConfig=$(/home/nginx_config/nginx/sbin/nginx -t 2>&1)
successFlag="successful"

if [[ $checkConfig =~ $successFlag ]]
then
    reload=$(/home/nginx_config/nginx/sbin/nginx -s reload 2>&1)
    if [[ $reload =~ "error" ]]
    then
        echo -e "重启nginx失败：\n" $reload
        echo "尝试启动nginx"
        startNginx=$(/home/nginx_config/nginx/sbin/nginx 2>&1)
        if [[ $startNginx == "" ]]
        then 
            echo "启动nginx成功"
        else 
            echo -e "启动nginx失败：\n" $startNginx
        fi   
    else 
        echo "重启nginx成功"
    fi
else
    echo -e "检查nginx配置有误：\n" $checkConfig
fi

sh /home/nginx_config/html/web/restart.sh

