此目录为 `nginx` 安装目录。

在 `config` 目录中的所有配置都是相对路径，基于 `nginx` 安装在这里。

如果想要修改当然也可以，记得吧 `config` 目录中的所有相对路径都修改一下。

> 注意：安装后需要在nginx.conf文件内添加以下`include ../../config/*.conf;`。
要添加在http块中。否则无法生效。

