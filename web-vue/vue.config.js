const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer: {
    port: 8080,
    proxy: {
      '/api': {
        target: 'http://web.test.itmtr.cn',
        ws: true,
        changeOrigin: true
      },
      '/websocket': {
        target: 'ws://web.test.itmtr.cn',
        ws: true,
        changeOrigin: true
      },
    }
  },
  transpileDependencies: true
})
