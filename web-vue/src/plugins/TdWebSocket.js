
export default (url, config) => {
    if (!url) {
        throw new Error("请输入WebSocket连接");
    }

    var ws;

    function initWebSocket() {
        initiativeClose();

        if ('WebSocket' in window) {
            if (config) {
                ws = new WebSocket(url);
                //连接发生错误的回调方法
                ws.onerror = config.onError || function (e) {
                    console.log(e);
                };
                ws.onopen = function () {
                    startHeartbeatInterval();
                    if (config.onOpen) {
                        config.onOpen();
                    }
                };
                ws.onmessage = function (event) {
                    // console.log("WebSocket收到消息:", event.data);
                    if (event.data) {
                        if (config.onMessage) {
                            config.onMessage(event.data);
                        }
                    }
                };
                ws.onclose = function () {
                    console.log("WebSocket连接关闭!");
                    // 如果页面未关闭就断开连接 10s后重连
                    startReconnectTimer();
                    if (config.onClose) {
                        config.onClose();
                    }
                };
            }
        } else {
            console.log("当前浏览器不支持WebSocket");
        }
    }

    /**
     * 心跳计时器
     */
    var heartbeatInterval;

    function startHeartbeatInterval() {
        endHeartbeatInterval();
        // 30秒发送一次心跳
        heartbeatInterval = setInterval(function () {
            sendMsg("heartbeat");
        }, 30000);
    }

    function endHeartbeatInterval() {
        if (heartbeatInterval) {
            clearInterval(heartbeatInterval);
            heartbeatInterval = null;
        }
    }

    /**
     * 重新连接计时器
     */
    var reconnectTimer;

    function startReconnectTimer() {
        endReconnectTimer();
        // 当连接关闭时 20秒后重试
        reconnectTimer = setTimeout(function () {
            initWebSocket();
        }, 20000);
    }

    function endReconnectTimer() {
        if (reconnectTimer) {
            clearTimeout(reconnectTimer);
            reconnectTimer = null;
        }
    }

    /**
     * 向服务端发送消息
     * @param message 消息内容 不会进行任何处理
     */
    function sendMsg(message) {
        if (ws && message) {
            ws.send(message);
        }
    }

    /**
     * 主动关闭不会触发重新连接
     */
    function initiativeClose() {
        if (ws) {
            ws.close();
        }
        endReconnectTimer();
        endHeartbeatInterval();
    }

    // 初始化
    initWebSocket();

    return {
        sendMsg: sendMsg,
        close: initiativeClose
    };
}
