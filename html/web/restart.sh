pids=$(ps -ef | grep web-serve.jar | grep -v grep | awk '{print $2}')

for pid in $pids
do 

kill -9 $pid
echo  "killed $pid"

done

cd /home/nginx_config/html/web
nohup java -Xms512M -Xmx512M -Xmn308M -Xss1M -XX:MetaspaceSize=128M -XX:MaxMetaspaceSize=128M -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=92 -XX:+UseCMSCompactAtFullCollection -XX:CMSFullGCsBeforeCompaction=0 -XX:+CMSParallelInitialMarkEnabled -XX:+CMSScavengeBeforeRemark -XX:+DisableExplicitGC -XX:+PrintGCDetails -XX:+HeapDumpOnOutOfMemoryError -jar web-serve.jar >> /dev/null 2>&1&
echo "start"

