package com.test.web.websocket;

import cn.hutool.http.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * <p>WebSocket服务</p>
 *
 * @author mtr
 * @since 2022-04-19
 */
@Slf4j
@Component
@ServerEndpoint("/websocket")
public class WebSocketServer {

    private Session session;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        log.info("websocket打开连接");
        this.session = session;
    }

    @OnMessage
    public void onMessage(String message) {
        log.info("websocket收到消息：{}", message);
        this.session.getAsyncRemote().sendText(HttpUtil.get("https://api.lovelive.tools/api/SweetNothings"));
    }

    @OnClose
    public void onClose() {
        log.info("websocket关闭连接");
    }

}
