package com.test.web.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>测试控制器</p>
 *
 * @author mtr
 * @since 2022-04-19
 */
@RestController
@CrossOrigin({"*"})
public class TestController {

    @GetMapping()
    public String main() {
        return HttpUtil.get("https://v1.hitokoto.cn?encode=text");
    }

    @GetMapping("/api")
    public String one() {
        return JSONUtil.parseObj(HttpUtil.get("https://v1.hitokoto.cn")).toStringPretty();
    }

}
