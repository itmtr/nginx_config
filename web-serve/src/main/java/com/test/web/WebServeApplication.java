package com.test.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServeApplication.class, args);
    }

}
