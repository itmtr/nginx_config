# 项目介绍

`nginx` 的一些简单常用的配置，自己学习的一些记录。

# 目录结构

```
nginx_config
├── config      // 主要的配置
├── html        // 访问的资源
├── nginx       // nginx安装目录 需要自己安装
├── tool        // 使用到的工具
├── web-serve   // 服务端项目
├── web-vue     // vue单页面前端项目
```

# 使用方法

克隆该仓库到本地。

将 `nginx` 安装在 `nginx` 目录下。

在`nginx`的`conf`文件夹内的`nginx.conf`文件内添加以下`include ../../config/*.conf;`。
注意：要添加在`http`块中。

按照 `hosts配置.txt` 中的配置修改 `hosts` 文件，使得测试域名在本地可以访问。

启动 `nginx` 即可访问。

# 查看文档

[https://blog.itmtr.cn/archives/nginx-config](https://blog.itmtr.cn/archives/nginx-config)


